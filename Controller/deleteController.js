let con = require('../connection');

// module.exports.deleteCity = (req, res) => {
//     var uId = req.params.id;
//     var sql = `Select CityName from CityList where city_id = ${uId};`
//     con.query(sql, function(err, result, fields) {
//        if(result.length === 0) {
//          res.status(400).json({
//            "status" : false,
//            "message" : `ID ${uId} is not valid`
//          });
//        } else {
//          var sql = `Delete from CityList where city_id = ${uId};`
//          con.query(sql, function(err, result, fields) {
//            res.status(200).json({
//              "status" : true,
//              "message" : `Deleted City successfully with id: ${uId}`
//            });
//          })
//        }
//     });
//  }

module.exports.deleteCityKnex = (req, res) => {
  var uId = req.params.id;
  global.knexConnection("CityTableKnex")
  .where({city_id : uId})
  .del()
  .then(data => {
    console.log
    res.status(200).json({
      "status" : true,
      "message" : "Done",
      "data" : data
    })
  }).catch(err => {
    res.status(500).json({
      "status" : false,
      "message" : "Something went wrong!.",
      "data" : err
    })
  }) 
}


//  module.exports.deleteHotel = (req, res) => {
//     var uId = req.params.id;
//     var sql = `Select HotelName from HotelList where hotel_id = ${uId};`
//     con.query(sql, function(err, result, fields) {
//        if(result.length === 0) {
//          res.status(400).json({
//            "status" : false,
//            "message" : `ID ${uId} is not valid`
//          });
//        } else {
//          var sql = `Delete from HotelList where hotel_id = ${uId};`
//          con.query(sql, function(err, result, fields) {
//            res.status(200).json({
//              "status" : true,
//              "message" : `Deleted Hotel successfully with id: ${uId}`
//            });
//          })
//        }
//     });
//   }

module.exports.deleteHotelKnex = (req, res) => {
  var uId = req.params.id;
  global.knexConnection("HotelTableKnex")
  .where({hotel_id : uId})
  .del()
  .then(data => {
    console.log
    res.status(200).json({
      "status" : true,
      "message" : "Done",
      "data" : data
    })
  }).catch(err => {
    res.status(500).json({
      "status" : false,
      "message" : "Something went wrong!.",
      "data" : err
    })
  }) 
  
}

module.exports.deleteMenuTypeKnex = (req, res) => {
  var uId = req.params.id;
  global.knexConnection("MenuTableKnex")
  .where({menu_id : uId})
  .del()
  .then(data => {
    console.log
    res.status(200).json({
      "status" : true,
      "message" : "Done",
      "data" : data
    })
  }).catch(err => {
    res.status(500).json({
      "status" : false,
      "message" : "Something went wrong!.",
      "data" : err
    })
  }) 
}

module.exports.deleteDishKnex = (req, res) => {
  var uId = req.params.id;
  global.knexConnection("DishTableKnex")
  .where({dish_id : uId})
  .del()
  .then(data => {
    console.log
    res.status(200).json({
      "status" : true,
      "message" : "Done",
      "data" : data
    })
  }).catch(err => {
    res.status(500).json({
      "status" : false,
      "message" : "Something went wrong!.",
      "data" : err
    })
  }) 
}

  // module.exports.deleteMenuType = (req, res) => {
  //   var uId = req.params.id;
  //   var sql = `Select MenuName from MenuList where menu_id = ${uId};`
  //   con.query(sql, function(err, result, fields) {
  //      if(result.length === 0) {
  //        res.status(400).json({
  //          "status" : false,
  //          "message" : `ID ${uId} is not valid`
  //        });
  //      } else {
  //        var sql = `Delete from MenuList where menu_id = ${uId};`
  //        con.query(sql, function(err, result, fields) {
  //          res.status(200).json({
  //            "status" : true,
  //            "message" : `Deleted Menu successfully with id: ${uId}`
  //          });
  //        })
  //      }
  //   });
  // }

  module,exports.deleteDish = (req, res) => {
    var uId = req.params.id;
    var sql = `Select DishName from DishList where dish_id = ${uId};`
    con.query(sql, function(err, result, fields) {
       if(result.length === 0) {
         res.status(400).json({
           "status" : false,
           "message" : `ID ${uId} is not valid`
         });
       } else {
         var sql = `Delete from DishList where dish_id = ${uId};`
         con.query(sql, function(err, result, fields) {
           res.status(200).json({
             "status" : true,
             "message" : `Deleted Dish successfully with id: ${uId}`
           });
         })
       }
    });
  }


