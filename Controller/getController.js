let con = require('../connection');

// module.exports.GetData = (req, res) => {
//     var dataParam = req.params.id;
//     var sql = "Select * from "+dataParam+" ";
//     con.query(sql,function (err, result, fields) {
//       if (err) throw err;
//       res.status(200).send(result);
//     });
//   }

//knex
module.exports.GetCityDataKnex = (req, res) => {
    var dataParam = req.params.id;
    global.knexConnection("CityTableKnex").then(data=>{
          console.log(res);
          res.status(200).json({
            "status" : true,
            "message" : "City Added",
            "data":data
          });  
      }).catch(err=>{
          console.log(err);
          res.status(500).json({
            "status" : false,
            "message" : "Request denied !",
            "data":err
          }); 
      })
    // con.query(sql,function (err, result, fields) {
    //   if (err) throw err;
    //   res.status(200).send(result);
    // });
  }

  module.exports.GetHotelDataKnex = (req, res) => {
    var dataParam = req.params.id;
    global.knexConnection("HotelTableKnex").then(data=>{
          console.log(res);
          res.status(200).json({
            "status" : true,
            "message" : "Request accepted !",
            "data":data
          });  
      }).catch(err=>{
          console.log(err);
          res.status(500).json({
            "status" : false,
            "message" : "Request denied !",
            "data":err
          }); 
      })
    // con.query(sql,function (err, result, fields) {
    //   if (err) throw err;
    //   res.status(200).send(result);
    // });
  }

  module.exports.GetMenuDataKnex = (req, res) => {
    
    global.knexConnection("MenuTableKnex").then(data=>{
          console.log(res);
          res.status(200).json({
            "status" : true,
            "message" : "Request accepted !",
            "data":data
          });  
      }).catch(err=>{
          console.log(err);
          res.status(500).json({
            "status" : false,
            "message" : "Request denied !",
            "data":err
          }); 
      })
    // con.query(sql,function (err, result, fields) {
    //   if (err) throw err;
    //   res.status(200).send(result);
    // });
  }

  module.exports.GetDishDataKnex = (req, res) => {
    
    global.knexConnection("DishTableKnex").then(data=>{
          console.log(res);
          res.status(200).json({
            "status" : true,
            "message" : "Request accepted !",
            "data":data
          });  
      }).catch(err=>{
          console.log(err);
          res.status(500).json({
            "status" : false,
            "message" : "Request denied !",
            "data":err
          }); 
      })
    // con.query(sql,function (err, result, fields) {
    //   if (err) throw err;
    //   res.status(200).send(result);
    // });
  }

module.exports.GetPaginatedData = (req, res) => {
  let limit = req.body['limit'];
  let count = req.body['count'];

  let queryData = `select * from DishList LIMIT ${limit}, ${count};`
  con.query(queryData, (err, result, fields) => {
    res.status(200).send(result);
  });

}  

//Android TV
module.exports.getAndroidTVHomeData = (req, res) => {
    res.status(200).json({
        "data" : [
            {
                "category" : "Hollywood",
                "PosterData" : [
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/Doorbeen.jpg",
                        "name" : "Doorbeen",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio One",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/iksundhuhundasi.jpg",
                        "name" : "Saand ki aakh",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio Two",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/laavanphere.jpg",
                        "name" : "Laavansphere",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio Three",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/Parahuna.JPG",
                        "name" : "Parahuna",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio Four",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    
                ]
            },
            {
                "category" : "Bollywood",
                "PosterData" : [
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/Doorbeen.jpg",
                        "name" : "Movie",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio One",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/iksundhuhundasi.jpg",
                        "name" : "Movie",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio Two",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/laavanphere.jpg",
                        "name" : "Movie",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio Three",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/Parahuna.JPG",
                        "name" : "Movie",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio Four",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                ]
            },
            {
                "category" : "Tollywood",
                "PosterData" : [
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/Doorbeen.jpg",
                        "name" : "Movie",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio One",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/iksundhuhundasi.jpg",
                        "name" : "Movie",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio Two",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/laavanphere.jpg",
                        "name" : "Movie",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio Three",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/Parahuna.JPG",
                        "name" : "Movie",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio Four",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    
                ]
            },
            {
                "category" : "South Indian",
                "PosterData" : [
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/Doorbeen.jpg",
                        "name" : "Movie",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio One",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/iksundhuhundasi.jpg",
                        "name" : "Movie",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio Two",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/laavanphere.jpg",
                        "name" : "Movie",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio Three",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    {
                        "cardImageUrl" : "https://bnott.binarynumbers.io/images/movies/Parahuna.JPG",
                        "name" : "Movie",
                        "desc" : "Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est, Donec tristique, orci sed semper lacinia, quam erat rhoncus massa, non congue tellus est",
                        "studio" : "Studio Four",
                        "bgImageUrl" : "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg"
                    },
                    
                ]
            },
        ]
    })
}

//Netflix
module.exports.getHomeData = (req, res) => {
  res.status(200).json({
    "Status": true,
    "Message": "",
    "header": {
        "ID": 5,
        "background_image": "https://bnott.binarynumbers.io/images/Laung-laachi-575-275-1.jpg",
        "title": "Laung Laachi",
        "subtitle": "Watch movie now!"
    },
    "preview": {
        "header": "Previews",
        "data": [
            "https://bnott.binarynumbers.io/images/movies/Doorbeen.jpg",
            "https://bnott.binarynumbers.io/images/movies/iksundhuhundasi.jpg",
            "https://bnott.binarynumbers.io/images/movies/laavanphere.jpg",
            "https://bnott.binarynumbers.io/images/movies/Parahuna.JPG"
        ]
    },
    "mylist": {
        "heading": "My List",
        "showBottomSheet": true,
        "data": [
            {
                "ID": 1,
                "img": "https://bnott.binarynumbers.io/images/movies/Doorbeen.jpg",
                "netflixOriginal": true,
                "netflixOriginalImg": "https://bnott.binarynumbers.io/images/netfix.png",
                "top10": false,
                "newEpisodes": true,
                "popupTitle": "Doorbeen",
                "popupSubTitle": "Watch now",
                "popupPlot": "Watch movie now"
            },
            {
                "ID": 2,
                "img": "https://bnott.binarynumbers.io/images/movies/iksundhuhundasi.jpg",
                "netflixOriginal": false,
                "netflixOriginalImg": "",
                "top10": true,
                "newEpisodes": true,
                "popupTitle": "Ik sundhu hunda si",
                "popupSubTitle": "Watch Now!",
                "popupPlot": "Watch movie now"
            },
            {
                "ID": 3,
                "img": "https://bnott.binarynumbers.io/images/movies/laavanphere.jpg",
                "netflixOriginal": false,
                "netflixOriginalImg": "",
                "top10": true,
                "newEpisodes": false,
                "popupTitle": "Laavan Phere",
                "popupSubTitle": "Watch Now!",
                "popupPlot": "Watch movie now"
            },
            {
                "ID": 4,
                "img": "https://bnott.binarynumbers.io/images/movies/Parahuna.JPG",
                "netflixOriginal": true,
                "netflixOriginalImg": "https://bnott.binarynumbers.io/images/netfix.png",
                "top10": false,
                "newEpisodes": false,
                "popupTitle": "Parahuna",
                "popupSubTitle": "Watch Now!",
                "popupPlot": "Watch now"
            }
        ]
    },
    "newRelease": {
        "heading": "New Releases",
        "showBottomSheet": true,
        "data": [
            {
                "ID": 4,
                "img": "https://bnott.binarynumbers.io/images/movies/Parahuna.JPG",
                "netflixOriginal": false,
                "netflixOriginalImg": "",
                "top10": false,
                "newEpisodes": true,
                "popupTitle": "Parahuna",
                "popupSubTitle": "Watch Season 5 now",
                "popupPlot": "Watch Season 1 now"
            },
            {
                "ID": 3,
                "img": "https://bnott.binarynumbers.io/images/movies/laavanphere.jpg",
                "netflixOriginal": true,
                "netflixOriginalImg": "https://bnott.binarynumbers.io/images/netfix.png",
                "top10": false,
                "newEpisodes": true,
                "popupTitle": "Laavan Phere",
                "popupSubTitle": "Watch Now!",
                "popupPlot": "Watch Season 1 now"
            },
            {
                "ID": 2,
                "img": "https://bnott.binarynumbers.io/images/movies/iksundhuhundasi.jpg",
                "netflixOriginal": true,
                "netflixOriginalImg": "https://bnott.binarynumbers.io/images/netfix.png",
                "top10": true,
                "newEpisodes": true,
                "popupTitle": "Ik sundhu hunda si",
                "popupSubTitle": "Watch Now!",
                "popupPlot": "Watch Season 1 now"
            },
            {
                "ID": 1,
                "img": "https://bnott.binarynumbers.io/images/movies/Doorbeen.jpg",
                "netflixOriginal": true,
                "netflixOriginalImg": "https://bnott.binarynumbers.io/images/netfix.png",
                "top10": true,
                "newEpisodes": true,
                "popupTitle": "Doorbeen",
                "popupSubTitle": "Watch Season 1 now",
                "popupPlot": "Watch Season 1 now"
            }
        ]
    },
    "trendingNow": {
        "heading": "Trending Now",
        "showBottomSheet": true,
        "data": [
            {
                "ID": 1,
                "img": "https://bnott.binarynumbers.io/images/movies/Doorbeen.jpg",
                "netflixOriginal": true,
                "netflixOriginalImg": "https://bnott.binarynumbers.io/images/netfix.png",
                "top10": true,
                "newEpisodes": false,
                "popupTitle": "Doorbeen",
                "popupSubTitle": "Watch Season 1 now",
                "popupPlot": "Watch Season 1 now"
            },
            {
                "ID": 2,
                "img": "https://bnott.binarynumbers.io/images/movies/iksundhuhundasi.jpg",
                "netflixOriginal": true,
                "netflixOriginalImg": "https://bnott.binarynumbers.io/images/netfix.png",
                "top10": false,
                "newEpisodes": true,
                "popupTitle": "Ik sundhu hunda si",
                "popupSubTitle": "Watch Now!",
                "popupPlot": "Watch Season 1 now"
            },
            {
                "ID": 3,
                "img": "https://bnott.binarynumbers.io/images/movies/laavanphere.jpg",
                "netflixOriginal": true,
                "netflixOriginalImg": "https://bnott.binarynumbers.io/images/netfix.png",
                "top10": false,
                "newEpisodes": true,
                "popupTitle": "Laavan Phere",
                "popupSubTitle": "Watch Now!",
                "popupPlot": "Watch Season 1 now"
            },
            {
                "ID": 4,
                "img": "https://bnott.binarynumbers.io/images/movies/Parahuna.JPG",
                "netflixOriginal": true,
                "netflixOriginalImg": "https://bnott.binarynumbers.io/images/netfix.png",
                "top10": false,
                "newEpisodes": false,
                "popupTitle": "Parahuna",
                "popupSubTitle": "Watch Season 1 now",
                "popupPlot": "Watch Season 1 now"
            }
        ]
    }
});
}

