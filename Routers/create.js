
var express = require('express');
let router = express.Router()
let con = require('../connection');
let addController = require('../Controller/createController');

router.route('/addNewCityKnex').post(addController.createCityKnex);
  
router.route('/addHotelKnex').post(addController.createHotelKnex);
  
router.route('/addMenuTypeKnex').post(addController.createMenuTypeKnex);
  
router.route('/addNewDishKnex').post(addController.createDishKnex);


  router.get('/test', (req,res) =>res.json({msg: 'It Works'}));

  module.exports = router;

