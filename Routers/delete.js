let express = require('express');
let deleteRouter = express.Router();
let deleteController = require('../Controller/deleteController');

deleteRouter.route('/deleteCityKnex/:id').delete(deleteController.deleteCityKnex);
 
 deleteRouter.route('/deleteHotelKnex/:id').delete(deleteController.deleteHotelKnex);
 
 deleteRouter.route('/deleteMenuKnex/:id').delete(deleteController.deleteMenuTypeKnex);
 
 deleteRouter.route('/deleteDishKnex/:id').delete(deleteController.deleteDish);


 module.exports = deleteRouter;
