
let express = require('express');
let getRouter = express.Router();
let getDataFunction = require('../Controller/getController');



getRouter.route('/getPaginatedData').post(getDataFunction.GetPaginatedData);
getRouter.route('/getHomeData').get(getDataFunction.getHomeData);
getRouter.route('/getTVData').get(getDataFunction.getAndroidTVHomeData);
getRouter.route('/getHotelDataKnex').get(getDataFunction.GetHotelDataKnex);
getRouter.route('/getCityDataKnex').get(getDataFunction.GetHotelDataKnex);
getRouter.route('/getMenuDataKnex').get(getDataFunction.GetMenuDataKnex);
getRouter.route('/getDishDataKnex').get(getDataFunction.GetDishDataKnex);

module.exports = getRouter; 
