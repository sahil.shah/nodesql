let express = require('express');
let updateRouter = express.Router();
let updateController = require('../Controller/updateController');

//updateRouter.route('/updateCity/:id').put(updateController.updateCity);
  
//  updateRouter.route('/updateHotel/:id').put(updateController.updateHotel);
  
  updateRouter.route('/updateMenuTypeKnex/:id').put(updateController.updateMenuTypeKnex);
  
  updateRouter.route('/updateNewDishKnex/:id').put(updateController.updateDishKnex);

  updateRouter.route('/updateCityTableKnex/:id').put(updateController.updateCityTableKnex);

  updateRouter.route('/updateHotelTableKnex/:id').put(updateController.updateHotelKnex);

module.exports = updateRouter;
