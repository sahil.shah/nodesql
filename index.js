

var express = require('express');
const app = express();
var mysql = require('mysql');
var multer  = require('multer')

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    const ext = file.mimetype.split('/')[1];
    cb(null, `user-${Date.now()}.${ext}`);
  }
})

var upload = multer({ storage : storage })
let createRouter = require('./Routers/create');
let getRouter = require('./Routers/getData');
let updateRouter = require('./Routers/update');
let deleteRouter = require('./Routers/delete');
let regLogin = require('./storePassword');
// let con = require('./connection');

const configObj = require('./knexfile');
var knex = require('knex')(configObj.development);

global.knexConnection=knex;


// global.knexConnection("m_user").then(res=>{
//   console.log(res)
// })


// var con = mysql.createConnection({
//   host: "localhost",
//   user: "root",
//   password: "Sahil@09"
// });

// con.connect(function(err) {
//   if (err) throw err;
//   console.log("Connected!");

//   // con.query("use hotel", function(err, res, fields) {
//   //     console.log(res);
//   // });
// });

bodyparser = require('body-parser');
app.use(bodyparser.json());
app.use('/api', createRouter);
app.use('/api', getRouter);
app.use('/api', updateRouter);
app.use('/api', deleteRouter);
app.use('/api', regLogin);

app.post('/profile', upload.single('Image'), function (req, res, next) {
  console.log(req.file);
  res.status(200).json({
    "name" : req.body['name'],
  });
})

const port = 3337;
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});



//CallBack
// console.log("Before");
// getUserRepo("Sahil", displayUserRepo)
// console.log("After");

// function displayUserRepo(list) {
//   console.log(list)
// }

// function getUserRepo(username, callBack) {
//   setTimeout(() => {
//     callBack(['repo 1', 'repo 2', 'repo 3'])
//   }, 1500)
// }


//Prmoise
// console.log("Before")
// const p = getUserRepo("Sahil")
// p.then(result => console.log(result))
// console.log("After")

// function getUserRepo(username) {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//     resolve(['repo 1', 'repo 2', 'repo 3'])
//   }, 1500)
//   })
// }

// //async await

// async function displayArray() {
//   console.log("before")
//   const data = await getUserRepo("Some String")
//   console.log("Bhai aagaya data", data)
// }

// displayArray()

