
exports.up = async function(knex) {
    await knex.schema.createTable("CityTableKnex", (table) => {
        table.increments("city_id").primary();
        table.string("city_name", 100);
    })
};

exports.down = async function(knex) {
    await knex.schema.dropTable("CityTableKnex");
};
