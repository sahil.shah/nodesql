
exports.up = async function(knex) {
    await knex.schema.createTable("HotelTableKnex", (table) => {
        table.increments("hotel_id").primary();
        table.string("hotel_name", 100);
        table.string("city_id", 100);
        table.string("is_active", 100);
    })
};

exports.down = async function(knex) {
    await knex.schema.dropTable("HotelTableKnex");
};
