
exports.up = async function(knex) {
    await knex.schema.createTable("MenuTableKnex", (table) => {
        table.increments("menu_id").primary();
        table.string("menu_name", 100);
        table.string("is_active", 100);
    })
};

exports.down = async function(knex) {
    await knex.schema.dropTable("MenuTableKnex");
};
