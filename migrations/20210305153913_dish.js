
exports.up = async function(knex) {
    await knex.schema.createTable("DishTableKnex", (table) => {
        table.increments("dish_id").primary();
        table.string("dish_name", 100);
        table.string("is_active", 100);
        table.string("menu_id", 100);
        table.string("hotel_id", 100);
        table.string("price", 100);
    })
};

exports.down = async function(knex) {
    await knex.schema.dropTable("DishTableKnex");
};
